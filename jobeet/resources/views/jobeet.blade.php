<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style>
        .container{
            margin: 0px;
            padding: 0px;
            margin: auto;
        }
        .navbar-inverse ul li{
            width: 130px;
            text-align: center;
        }
        .menu{
            width: 100%;
            height: auto;
        }
        .col-lg-8 {
            margin: 0px;
            padding: 0px;
        }
    </style>
</head>
<body background="1.jpg">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-inverse col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="jobeet.html">Jobeet</a></li>
                    <li><a href="post_job.html">Post New Job</a></li>
                    <li><a href="find_job.html">Find Job</a></li>
                    <li><a href="user.html">User</a></li>
                    <li><a href="apply.html">Apply</a></li>
                </ul>
            </nav>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 text-center">
                <h1>New Job</h1>
                <table class="table">
                    <thead style="background-color: black; color: white;">
                        <tr>
                            <th style="text-align: center;">Name</th>
                            <th style="text-align: center;">Posted</th>
                            <th style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>IT Job</td>
                            <td>2016/04/21</td>
                            <td>Apply</td>
                        </tr>      
                        <tr class="success">
                            <td>IT Job</td>
                            <td>2016/04/21</td>
                            <td>Apply</td>
                        </tr>
                        <tr class="danger">
                            <td>IT Job</td>
                            <td>2016/04/21</td>
                            <td>Apply</td>
                        </tr>
                        <tr class="info">
                            <td>IT Job</td>
                            <td>2016/04/21</td>
                            <td>Apply</td>
                        </tr>
                        <tr class="warning">
                            <td>IT Job</td>
                            <td>2016/04/21</td>
                            <td>Apply</td>
                        </tr>
                        <tr class="active">
                            <td>IT Job</td>
                            <td>2016/04/21</td>
                            <td>Apply</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>  
</body>
</html>